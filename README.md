# @bazoj/api #

@bazoj/api is a library for simplifying some basic checks.

### How do I get set up? ###

Install the specific wanted tool using

```
npm install @bazoj/checks
```

Import in the code and use.

```
import <tool name> from '@bazoj/api';
<tool name>();
```

... That's it. :]
