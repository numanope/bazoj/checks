async function isOnline() {
	let onlineStatus = false;
	if (typeof window !== 'undefined' && typeof navigator !== 'undefined') {
		if (navigator.onLine) {
			onlineStatus = true;
		}
	}
	else if (typeof process !== 'undefined') {
		const isOnline = (await import('is-online')).default;
		if (await isOnline()) {
			onlineStatus = true;
		}
	}
	else {
		throw new Error('Unsupported environment');
	}

	return onlineStatus;
}

export default isOnline;
